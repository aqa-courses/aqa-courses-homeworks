from fixtures.school_fixtures import *


def test_add_student_to_class(school1, add_class):
    school1.add_students_to_class("1a", [Student("test123", "test123"), Student("Test2", "Test2")])
    assert school1.school_budget() == -2900


def test_remove_student(school1, add_class):
    school1.add_students_to_class("1a", [Student("test123", "test123"), Student("Test2", "Test2")])
    school1.remove_students_from_class("1a", [Student("test123", "test123")])
    assert school1.school_budget() == -3000


def test_check_for_duplicate(school1, add_class):
    school1.add_students_to_class("1a", [Student("test123", "test123"), Student("Test2", "Test2")])
    test = school1.add_students_to_class("1a", [Student("test123", "test123")])
    assert test is True, \
        f"Student already present"


def test_students_are_needed(school1, add_class):
    school1.add_students_to_class("1a", [Student("test123", "test123"), Student("Test2", "Test2")])
    st_are_needed = school1.needed_students_count()
    assert st_are_needed == 29

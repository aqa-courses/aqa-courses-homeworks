from abc import abstractmethod
from personas.person import Person


class Personal(Person):
    def __init__(self, name, surname):
        super().__init__(name, surname)

    @abstractmethod
    def get_rate(self):
        pass


class Teacher(Personal):
    def __init__(self, name, surname):
        super().__init__(name, surname)

    def get_rate(self):
        return 500


class Director(Personal):
    def __init__(self, name, surname):
        super().__init__(name, surname)

    def get_rate(self):
        return 1000


class ViceDirector(Personal):
    def __init__(self, name, surname):
        super().__init__(name, surname)

    def get_rate(self):
        return 700


class Cleaner(Personal):
    def __init__(self, name, surname):
        super().__init__(name, surname)

    def get_rate(self):
        return 200

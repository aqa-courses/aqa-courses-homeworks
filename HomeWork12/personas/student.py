from personas.person import Person


class Student(Person):
    @staticmethod
    def get_payment_rate():
        return 100

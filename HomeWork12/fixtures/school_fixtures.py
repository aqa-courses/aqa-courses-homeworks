import pytest
from school.school1 import *


@pytest.fixture
def school1():
    director1 = Director("Test2", "Test2")
    vice_director = ViceDirector("Test3", "Test3")
    cleaner1 = Cleaner("Test4", "Test4")
    cleaner2 = Cleaner("Test5", "Test5")
    school = School(director1, vice_director, [cleaner1, cleaner2])
    return school


@pytest.fixture
def add_class(school1):
    new_class = school1.add_classes(
        [SchoolClasses("1a", Teacher("Vasia", "Test1")), SchoolClasses("2a", Teacher("Petia", "yest1"))])
    return new_class

from personas.personal import *
from personas.student import Student


class SchoolClasses:
    def __init__(self, class_name: str, teacher: Teacher, students: [Student] = None):
        self.class_name = class_name
        self.teacher = teacher
        self.students = students if students else list()

    def __len__(self):
        return len(self.students)

    def duplicate_check(self, student):
        for st in self.students:
            if st.name == student.name and st.surname == student.surname:
                print("Student already present")
                return True
        return False

    def add_students(self, students: [Student]):
        for student in students:
            if self.duplicate_check(student):
                continue
            self.students.append(student)
        return

    def remove_student(self, students: [Student]):
        for student in students:
            self.students = [st for st in self.students if st.name != student.name and st.surname != student.surname]

    def class_budget(self):
        res_budget = int(0)
        res_budget += Student.get_payment_rate() * len(self.students)
        res_budget -= self.teacher.get_rate()
        return res_budget

    def __str__(self):
        return f"""
            Teacher: {self.teacher}
            Students: {','.join([str(student) for student in self.students])}
            """

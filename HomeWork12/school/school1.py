from school.school_classes import *


class School:
    def __init__(self, director: Director, vice_director: ViceDirector, cleaners: [Cleaner] = None):
        self.director = director
        self.vice_director = vice_director
        self.cleaners = cleaners
        self.school_classes = []

    def duplicate_check(self, school_class):
        for cl in self.school_classes:
            if cl.class_name == school_class.class_name:
                print("Class already present")
                return True
        return False

    def add_classes(self, school_classes: [SchoolClasses]):
        for cl in school_classes:
            if self.duplicate_check(cl):
                continue
            self.school_classes.append(cl)
        return

    def add_students_to_class(self, class_name, students: [Student]):
        for cl in self.school_classes:
            if class_name == cl.class_name:
                cl.add_students(students)
                return True
        print("Клас не існує", class_name)
        return False

    def remove_students_from_class(self, class_name, students: [Student]):
        for cl in self.school_classes:
            if class_name == cl.class_name:
                cl.remove_student(students)
                return True
        print("Клас не існує", class_name)
        return False

    def school_budget(self):
        res_school_budget = int(0)
        res_school_budget -= self.director.get_rate()
        res_school_budget -= self.vice_director.get_rate()
        for cleaner in self.cleaners:
            res_school_budget -= cleaner.get_rate()
        for cl in self.school_classes:
            res_school_budget += cl.class_budget()
        return res_school_budget

    def needed_students_count(self):
        current_school_budget = self.school_budget()
        if current_school_budget >= 0:
            print("Нікого не треба. Бюджет =", current_school_budget)
            return 0
        students_needed = (-1 * current_school_budget) / Student.get_payment_rate()
        return students_needed
